from setuptools import setup

import version

setup(
    name='seeq',
    description='Seeq API and CLI tool',
    url='http://docs.seeq.io',
    author='Tomaz Berisa',
    email='tomaz.berisa@gmail.com',
    licence='Apache 2.0',
    version=version.version(),
    packages=['seeq'],
    install_requires=[
        'Click',
        'requests',
        'PyJWT'
    ],
    setup_requires=[
        'pytest-runner'
    ],
    tests_require=[
        'pytest',
        'responses'
    ],
    entry_points='''
        [console_scripts]
        seeq=seeq.cli:cli
    ''',
)