'''Created for setup.py'''

def version():
    with open('version/A-major', 'rt') as f:
        major = f.read().replace('\n', '')
    with open('version/B-minor', 'rt') as f:
        minor = f.read().replace('\n', '')
    with open('version/C-patch', 'rt') as f:
        patch = f.read().replace('\n', '')

    return '{}.{}.{}'.format(major, minor, patch)

if __name__=='__main__':
    print(version())
