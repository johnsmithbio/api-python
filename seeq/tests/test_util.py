import pytest
import jwt as pyjwt

import seeq

def test_ok():
    '''This should work'''
    jwt1 = seeq.util.jwt_signed(1, 'user_123', 'secret', url_template=None)
    jwt2 = seeq.util.jwt_signed(
        1, 'user_123', 'secret', 1000, url_template=None)

    assert pyjwt.decode(jwt1, 'secret') == {
        'study_id': 1,
        'external_id': 'user_123'
    }

    assert pyjwt.decode(jwt2, 'secret') == {
        'study_id': 1,
        'external_id': 'user_123',
        'subsidy_in_cents': 1000
    }

    # assert seeq.util.jwt_signed(1, 'user_123', 'secret') == b'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkeV9pZCI6MSwiZXh0ZXJuYWxfaWQiOiJ1c2VyXzEyMyJ9.s8yL1a4-nhg7F-sIkhbuFHez07ZE5DvRYp40C3qamc0'
    # assert seeq.util.jwt_signed(1, 'user_123', 'secret', 1000) == b'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkeV9pZCI6MSwic3Vic2lkeV9pbl9jZW50cyI6MTAwMCwiZXh0ZXJuYWxfaWQiOiJ1c2VyXzEyMyJ9.b86HSIanM7mjtagyNtv6Z2PPSjjgGQgSnkLYQEoKb3Q'
 
def test_study_id():
    # study_id should be integer
    with pytest.raises(TypeError):
        seeq.util.jwt_signed('abc', 'user_123', 'secret')

    # study_id should be integer >0
    with pytest.raises(ValueError):
        seeq.util.jwt_signed(-1, 'user_123', 'secret')

def test_external_id():
    # external_id should be integer or string
    seeq.util.jwt_signed(1, 'user_123', 'secret')
    seeq.util.jwt_signed(1, 1, 'secret')
    # float should fail
    with pytest.raises(TypeError):
        seeq.util.jwt_signed(1, 1.0, 'secret')

def test_study_api_key():
    # float should fail
    with pytest.raises(TypeError):
        seeq.util.jwt_signed(1, 'user_123', 1.0)

def test_subsidy_in_cents():
    # float should fail
    with pytest.raises(TypeError):
        seeq.util.jwt_signed(1, 'user_123', 'secret', 1.0)
    # negative integer should fail
    with pytest.raises(ValueError):
        seeq.util.jwt_signed(1, 'user_123', 'secret', -1)
